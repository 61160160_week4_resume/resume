import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget aboutMe = Container(
      padding: const EdgeInsets.all(28),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          // Expanded(
          // child:
          Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Container(
              //     padding: const EdgeInsets.only(),
              //     child: Text(
              //       'About Me',
              //       textAlign: TextAlign.center,
              //       style: TextStyle(fontWeight: FontWeight.bold),
              //     )),
              Text('About Me \n',
                  textAlign: TextAlign.right,
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              Text('ชื่อ: ภพธรรม์ แสงงาม ',
                  textAlign: TextAlign.left, style: TextStyle(fontSize: 17.0)),
              Text(
                'อายุ: 21',
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 17.0),
              ),
              Text('เพศ: ชาย',
                  textAlign: TextAlign.left, style: TextStyle(fontSize: 17.0))
            ],
          ),
          // ),
          Column(
            children: [
              SizedBox(
                width: 200.0,
                height: 200.0,
                child: Image.asset('images/611160160.jpg'),
              )
            ],
          )
        ],
      ),
    );
    Widget skill = Container(
      // child: Column(
      //   children: [
      //     Expanded(
      //       child: Row(
      //         children: [
      //           Container(
      //             child: Text('Skill'),
      //             padding: EdgeInsets.fromLTRB(10, 10, 5, 10),
      //             margin: EdgeInsets.all(10),
      //             decoration: BoxDecoration(
      //                 border: Border.all(width: 1, color: Colors.grey),
      //                 borderRadius:
      //                     const BorderRadius.all(const Radius.circular(8))),
      //           ),
      //         ],
      //       ),
      // ),

      child: Text('สกิล',
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      margin: EdgeInsets.fromLTRB(10, 0, 10, 15),
      decoration: BoxDecoration(
          color: Colors.yellow[100],
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: const BorderRadius.all(const Radius.circular(8))),
    );
    Widget iconslist = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'images/css.png',
            width: 100,
            height: 100,
          ),
          Image.asset(
            'images/html.png',
            width: 100,
            height: 100,
          ),
          Image.asset(
            'images/java-script.png',
            width: 100,
            height: 100,
          ),
          Image.asset(
            'images/vue.png',
            width: 100,
            height: 100,
          ),
          Image.asset(
            'images/flutter.png',
            width: 100,
            height: 100,
          ),
        ],
      ),
    );
    Widget titleprofile = Container(
      padding: const EdgeInsets.all(28),
      child: Row(
        children: [
          Expanded(
              child: Column(
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Text('ประวัติส่วนตัว',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
                width: 450,
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 15),
                decoration: BoxDecoration(
                    color: Colors.yellow[100],
                    border: Border.all(width: 1, color: Colors.grey),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(8))),
              )
            ],
          )),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Text('การศึกษา',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
                width: 450,
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 15),
                decoration: BoxDecoration(
                    color: Colors.yellow[100],
                    border: Border.all(width: 1, color: Colors.grey),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(8))),
              )
            ],
          ))
        ],
      ),
    );
    Widget dataprofile = Container(
      padding: const EdgeInsets.all(28),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Icon(
                      Icons.cake,
                    ),
                    Text('   7 กุมภาพันธ์ 2543'),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Icon(
                      Icons.emoji_flags,
                    ),
                    Text('    ไทย'),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Icon(
                      Icons.email,
                    ),
                    Text('     61160160@gmail.com'),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Icon(
                      Icons.contact_phone,
                    ),
                    Text('     098-xxxxxxx'),
                  ],
                ),
              ),
            ],
          ),
          Column(
            children: [
              SizedBox(
                width: 200.0,
                height: 200.0,
                child: Image.asset('images/Buu-logo11.png'),
              )
            ],
          )
        ],
      ),
    );
    return MaterialApp(
      home: Scaffold(
        body: ListView(
          children: [
            Image.asset(
              'images/resume.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            aboutMe,
            skill,
            iconslist,
            titleprofile,
            dataprofile
          ],
        ),
      ),
    );
  }
}
